from django import forms
from .models import MataKuliah

class InputForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = ['nama', 'dosen', 'sks', 'deskripsi', 'ruang', 'semester', 'tahun1', 'tahun2']
    
    my_widget = forms.TextInput(attrs={'class':'form-control', 'spellcheck':'false'})
    
    
    nama = forms.CharField(widget=my_widget, max_length=99)
    dosen = forms.CharField(widget=my_widget, max_length=99)
    sks = forms.IntegerField(max_value=24, widget=forms.NumberInput(attrs={'class':'form-control'}))
    deskripsi = forms.CharField(widget=my_widget, max_length= 250)
    ruang = forms.CharField(widget=my_widget, max_length=25)
    semester = forms.TypedChoiceField(
        coerce=str,
        choices=[('Gasal', 'Gasal'), ('Genap', 'Genap')],
        initial='Gasal',
        widget=forms.Select(attrs={'class':'custom-select mr-sm-2'})
    )
    tahun_choices = [
        (2016,2016),
        (2017,2017),
        (2018,2018),
        (2019,2019),
        (2020,2020),
        (2021,2021)
    ]
    tahun1 = forms.TypedChoiceField(coerce=int, choices=tahun_choices, initial=2020, widget=forms.Select(attrs={'class':'custom-select mr-sm-2'}))
    tahun2 = forms.TypedChoiceField(coerce=int, choices=tahun_choices, initial=2021, widget=forms.Select(attrs={'class':'custom-select mr-sm-2'}))