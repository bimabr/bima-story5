from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('pencet/', views.submit, name='submit'),
    path('delete/', views.delete, name='delete'),
    path('matkul/<int:value>', views.show_matkul, name='show')
]
