from django.shortcuts import render, reverse, get_object_or_404
from django.http import HttpResponseRedirect
from .forms import InputForm
from .models import MataKuliah

def home(request):
    # reset penomoran id dari 1
    count = 1
    id_baru = [i.pk for i in MataKuliah.objects.all()]
    for i in id_baru:
        MataKuliah.objects.filter(id=i).update(id=count)
        count+=1
    response = {
        'input_form' : InputForm,
        'matkul_obj' : MataKuliah.objects.all(),
        'condition' : False
    }
    return render(request, 'main/home.html', response)


def submit(request):
    form = InputForm(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

def delete(request):
    id = request.POST['dropdown']
    if request.method == 'POST':
        try:
            MataKuliah.objects.all().get(pk=id).delete()
        except MataKuliah.DoesNotExist:
            return HttpResponseRedirect('/')
        response = {
        'input_form' : InputForm,
        'matkul_obj' : MataKuliah.objects.all(),
        'condition' : True
        }
        return render(request, 'main/home.html', response)

def show_matkul(request, value):
    response = {
        'input_form' : InputForm,
        'matkul' : MataKuliah.objects.get(pk=value)
    }
    return render(request, 'main/matkul.html', response)