from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    SEMESTER = [
        ('Gasal', 'Gasal'),
        ('Gasal', 'Genap')
    ]

    nama = models.CharField(max_length = 99)
    dosen = models.CharField(max_length = 99)
    sks = models.IntegerField()
    deskripsi = models.CharField(max_length = 250)
    ruang = models.CharField(max_length = 25)
    semester = models.CharField(
        max_length=5,
        choices=SEMESTER,
    )
    tahun1 = models.IntegerField()
    tahun2 = models.IntegerField()