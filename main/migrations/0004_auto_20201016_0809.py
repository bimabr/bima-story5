# Generated by Django 3.1.2 on 2020-10-16 01:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20201014_0829'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matakuliah',
            name='semester',
            field=models.CharField(choices=[('Gasal', 'Gasal'), ('Gasal', 'Genap')], max_length=5),
        ),
    ]
