# Generated by Django 3.1.2 on 2020-10-14 00:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='matakuliah',
            name='semester',
            field=models.CharField(choices=[('Gasal', 'Gasal'), ('Gasal', 'Genap')], default='GASAL', max_length=5),
        ),
        migrations.AddField(
            model_name='matakuliah',
            name='tahun1',
            field=models.IntegerField(null=True, unique_for_year=True),
        ),
        migrations.AddField(
            model_name='matakuliah',
            name='tahun2',
            field=models.IntegerField(null=True, unique_for_year=True),
        ),
    ]
